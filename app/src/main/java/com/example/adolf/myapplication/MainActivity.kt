package com.example.adolf.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.adolf.myapplication.service.raw.RawPinkoiService
import com.fasterxml.jackson.databind.ObjectMapper
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        println(">>> onCreate")
        Retrofit.Builder()//
                .client(buildOttOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(ObjectMapper()))
                .baseUrl("https://www.pinkoi.com/apiv2/")
                .build().create(RawPinkoiService::class.java)
                .match("pinkoi")
                .subscribeOn(Schedulers.io())
                .doOnError{
                    e ->
                    println(">>> error " + e)
                }
                .subscribe { t ->
                    println(">>>> ok")
                    println(">>>> result " + t.toString())
        };
    }

    private fun buildOttOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val original = chain.request()

            // Request customization: add request headers
            val requestBuilder = original.newBuilder()
                    .header("EMAIL", "a3NjOTF1QGdtYWlsLmNvbQ==")

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        //change logLevelForDebug if you want see different http request logging
        val logLevelForDebug = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(if (BuildConfig.DEBUG)
            logLevelForDebug
        else
            HttpLoggingInterceptor.Level.NONE))
        return httpClient.build()
    }
}
