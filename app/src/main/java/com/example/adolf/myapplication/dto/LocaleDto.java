package com.example.adolf.myapplication.dto;

import com.example.adolf.myapplication.jackson.JsonDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

@JsonDto
class LocaleDto implements Serializable {

  private final String currency;
  private final String code;
  private final String geo_name;

  LocaleDto(@JsonProperty("currency") String currency, @JsonProperty("code") String code,
      @JsonProperty("geo_name") String geo_name) {
    this.currency = currency;
    this.code = code;
    this.geo_name = geo_name;
  }

  public String getCurrency() {
    return currency;
  }

  public String getCode() {
    return code;
  }

  public String getGeo_name() {
    return geo_name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    LocaleDto localeDto = (LocaleDto) o;

    if (currency != null ? !currency.equals(localeDto.currency) : localeDto.currency != null) {
      return false;
    }
    if (code != null ? !code.equals(localeDto.code) : localeDto.code != null) {
      return false;
    }
    return geo_name != null ? geo_name.equals(localeDto.geo_name) : localeDto.geo_name == null;
  }

  @Override
  public int hashCode() {
    int result = currency != null ? currency.hashCode() : 0;
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (geo_name != null ? geo_name.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "LocaleDto{" +
        "currency='" + currency + '\'' +
        ", code='" + code + '\'' +
        ", geo_name='" + geo_name + '\'' +
        '}';
  }
}
