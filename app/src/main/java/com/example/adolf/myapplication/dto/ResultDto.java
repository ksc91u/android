package com.example.adolf.myapplication.dto;


import com.example.adolf.myapplication.jackson.JsonDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonDto
class ResultDto implements Serializable {
    private final HitsDto hits;

    @JsonCreator
    ResultDto(@JsonProperty("hits") HitsDto hits) {
        this.hits = hits;
    }

    @Override
    public String toString() {
        return "ResultDto{" +
                ", hits=" + hits +
                '}';
    }

    @JsonDto
    public class HitsDto implements Serializable {
        private final int max_score;
        private final int total;
        private final List<HitDto> hitProducts;

        public HitsDto(@JsonProperty("max_score") int max_score, @JsonProperty("total") int total,
                       @JsonProperty("hitProducts") List<HitDto> hitProducts) {
            this.max_score = max_score;
            this.total = total;
            this.hitProducts = hitProducts;
        }

        @Override
        public String toString() {
            return "HitsDto{" +
                    "max_score=" + max_score +
                    ", total=" + total +
                    ", hitProducts=" + hitProducts +
                    '}';
        }
    }
}
