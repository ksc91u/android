package com.example.adolf.myapplication.dto;

import com.example.adolf.myapplication.jackson.JsonDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;


@JsonDto
public class SearchResultDto implements Serializable {
    private final LocaleDto locale;
    private final List<ResultDto> result;

    @JsonCreator
    SearchResultDto(@JsonProperty("locale") LocaleDto locale,
                    @JsonProperty("result") List<ResultDto> result
    ) {
        this.locale = locale;
        this.result = result;
    }

    public LocaleDto getLocale() {
        return locale;
    }

    public List<ResultDto> getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchResultDto that = (SearchResultDto) o;

        if (locale != null ? !locale.equals(that.locale) : that.locale != null) return false;
        return result != null ? result.equals(that.result) : that.result == null;
    }

    @Override
    public int hashCode() {
        int result1 = locale != null ? locale.hashCode() : 0;
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        return result1;
    }

    @Override
    public String toString() {
        return "SearchResultDto{" +
                "locale=" + locale +
                ", result=" + result +
                '}';
    }
}
