package com.example.adolf.myapplication.jackson;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * a meta jackson annotation include all common DTO json configuration
 * <p>
 * class that annotated with this annotation should apply:
 * <p>
 * 1. implements Serializable
 * 2. all fields are private and final
 * 3. generate all getters for properties need to be serialized in json
 * 4. generate equals(), hashCode() and, toString()
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
@JacksonAnnotationsInside
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public @interface JsonDto {
}
