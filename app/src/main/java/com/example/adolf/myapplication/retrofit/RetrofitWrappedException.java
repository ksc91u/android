package com.example.adolf.myapplication.retrofit;

import retrofit2.HttpException;

public class RetrofitWrappedException extends RuntimeException {
  RetrofitWrappedException(Throwable cause) {
    super(cause);
  }

  public boolean is404NotFound() {
    if (getCause() instanceof HttpException) {
      return ((HttpException) getCause()).code() == 404;
    }
    return false;
  }
}