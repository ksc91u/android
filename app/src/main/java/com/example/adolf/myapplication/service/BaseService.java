package com.example.adolf.myapplication.service;

import com.example.adolf.myapplication.retrofit.RetrofitWrappedException;
import java.util.Collections;
import java.util.List;
import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;

abstract class BaseService {
  protected static <T> Function<Throwable, SingleSource<? extends List<T>>> emptyListIf404NotFound() {
    return throwable -> {
      if (throwable instanceof RetrofitWrappedException) {
        if (((RetrofitWrappedException) throwable).is404NotFound()) {
          return Single.just(Collections.emptyList());
        }
      }
      return Single.error(throwable);
    };
  }

  protected static Function<Throwable, CompletableSource> continueIf404NotFound() {
    return throwable -> {
      if (throwable instanceof RetrofitWrappedException) {
        if (((RetrofitWrappedException) throwable).is404NotFound()) {
          return Completable.complete();
        }
      }
      return Completable.error(throwable);
    };
  }
}