package com.example.adolf.myapplication.service;


import com.example.adolf.myapplication.service.raw.RawPinkoiService;
import javax.inject.Inject;

public class PinkoiService extends BaseService{
  private final RawPinkoiService delegate;

  @Inject
  public PinkoiService(RawPinkoiService delegate) {
    this.delegate = delegate;
  }
}
