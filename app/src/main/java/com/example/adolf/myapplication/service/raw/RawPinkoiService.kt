package com.example.adolf.myapplication.service.raw

//https://www.pinkoi.com/apiv2/match?page=2&q=gay&_mode=search&facet=0

import com.example.adolf.myapplication.dto.SearchResultDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RawPinkoiService {

    @GET("match")
    fun match(@Query("q") q: String): Single<SearchResultDto>
}
